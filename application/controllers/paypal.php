<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class PayPal extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('errorhandler');
        $this->load->library('simplexml');
        $this->load->library('session');
        $this->load->library('notification');
        $this->load->model('user');
        $this->load->model('item');
        $this->load->helper('url');
    }

    /**
     * Stub action;
     */
    public function index() {
        echo 'here';
        echo 'here';
    }

    /**
     * Initializes the PayPal Adaptive Payments API and returns the URL for the user to authenticate with PayPal
     */
    public function buy() {
        $buyerId = intval($this->input->get('buyer'));
        $itemId = intval($this->input->get('item'));

        if($buyerId == 0 || $itemId == 0) {
            $this->errorhandler->setMessage('There was no data sent to the paypal/buy action.');
            $this->output($this->errorhandler->getError()->asXML());
            return;
        }

        // RETRIEVE THE BUYER USER OBJECT
        $buyer = $this->user->get(array("id" => $buyerId));
        if(!$buyer) {
            $this->errorhandler->setMessage('There was no user found with ID ' . $buyerId . '.');
            $this->output($this->errorhandler->getError()->asXML());
            return;
        }

        // RETRIEVE THE ITEM
        $item = $this->item->fetch($itemId);
        if(!$item) {
            $this->errorhandler->setMessage('There was no item found with ID ' . $itemId . '.');
            $this->output($this->errorhandler->getError()->asXML());
            return;
        }
        $item = array_shift($item);

        $seller = $this->user->get(array("id" => $item['user_id']));
        if(!$seller) {
            $this->errorhandler->setMessage('There was no user found with ID ' . $item['user_id'] . '.');
            $this->output($this->errorhandler->getError()->asXML());
            return;
        }

        $response = $this->makePayCall($buyer, $item, $seller);
        $this->session->set_flashdata('item', serialize($item));

        $message = '';
        switch($response->responseEnvelope->ack) {
            case 'Success':
                redirect('https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_ap-payment&paykey=' . $response->payKey);
                break;
            case 'Failure':
                foreach($response->error as $error) {
                    $message .= $error->message . "\n";
                }
                redirect('/paypal/cancel?message=' . urlencode($message));
                break;
        }
    }

    public function success() {
        $item = $this->session->flashdata('item');
        if(!$item) {
            return;
        }

        $item = unserialize($item);
        $seller = $this->user->get(array("id" => $item['user_id']));
        if(!$seller) {
            return;
        }
        $this->item->delete(array('id' => $item['id']));
        $this->notification->notify("webmaster@illogicalsystems.com", "Campus Garage Sale", $seller['email'], "Payment Successful", $this->load->view('emailtemplate', array("title" => "Congrats! You received payment for " . $item['name'], "content" => "<p>Log in to your PayPal account to review the payment details</p>"), TRUE));

        return $this->load->view('paypalmessage', array('title' => 'Success', 'message' => 'Your payment was successfully processed. Please press the back button to return to the item listing.'));
    }

    public function cancel() {
        $message = urldecode($this->input->get('message'));
        $title = "PayPal Error";
        if(!$message || $message == '') {
            $title = "Canceled";
            $message = "You canceled the transaction through PayPal. Please return to the item listing.";
        }

        return $this->load->view('paypalmessage', array('title' => $title, 'message' => $message));
    }


    /**
     * Set the output and content type
     * @param $output
     * @param string $type
     */
    protected function output($output, $type = 'text/xml') {
        $this->output->set_content_type($type);
        $this->output->set_output($output);
    }

    /**
     * Makes the Pay call to PayPal to receive the Pay App Key for authentication
     * @param array $buyer
     * @param array $item
     * @param array $seller
     * @return mixed
     */
    protected function makePayCall(array $buyer, array $item, array $seller) {
        $payCall = array(
            "actionType" => "PAY",    // Specify the payment action
            "currencyCode" => "USD",  // The currency of the payment
            "receiverList" => array(
                "receiver" => array(
                    "amount" => $item['price'],                    // The payment amount
                    "email" => $seller['paypal_email'],  // The payment Receiver's email address
                ),
            ),
            // Where the Sender is redirected to after approving a successful payment
            "returnUrl" => "http://booksale.illogicalsystems.com/paypal/success",
            // Where the Sender is redirected to upon a canceled payment
            "cancelUrl" => "http://booksale.illogicalystems.com/paypal/cancel",
            "requestEnvelope" => array(
                "errorLanguage" => "en_US",    // Language used to display errors
                "detailLevel" => "ReturnAll",   // Error detail level
            ),
        );

        if(isset($buyer['paypal_email']) && $buyer['paypal_email'] != '' ) {
            $payCall['senderEmail'] = $buyer['paypal_email'];
        }

        $jsonData = json_encode($payCall);

        // INITIALIZE CURL
        $ch = curl_init('https://svcs.sandbox.paypal.com/AdaptivePayments/Pay');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'X-PAYPAL-SECURITY-USERID: arcardenas-facilitator_api1.gmail.com',
                'X-PAYPAL-SECURITY-PASSWORD: 1376792852',
                'X-PAYPAL-SECURITY-SIGNATURE: AkuCRdBqHVMXR.ynZpbM.LGVUlMkApHUKmoCkUMQTFvhiJ0nnQzzHjnS',
                'X-PAYPAL-APPLICATION-ID: APP-80W284485P519543T',
                'X-PAYPAL-REQUEST-DATA-FORMAT: JSON',
                'X-PAYPAL-RESPONSE-DATA-FORMAT: JSON',
                'Content-Type: application/json',
                'Content-Length: ' . strlen($jsonData))
        );
        return json_decode(curl_exec($ch));
    }

}
