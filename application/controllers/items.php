<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Items extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('item');
        $this->load->model('user');
        $this->load->model('wishlist');
        $this->load->library('simplexml');
        $this->load->library('errorhandler');
        $this->load->library('notification');
        $this->output->set_content_type('text/xml');
    }

    public function index() {
        
    }

    public function fetchAll($campus_id = 0) {
        $this->simplexml->setNodes("items", "item");
        $xml = $this->simplexml->getXML($this->item->get(array("campus_id" => $campus_id)));
        $this->output->set_output($xml);
    }

    public function fetch($item_id = 0) {
        $this->simplexml->setNodes("items", "item");
        $xml = $this->simplexml->getXML($this->item->fetch($item_id));
        $this->output->set_output($xml);
    }

    public function add() {
        $params = (array) json_decode($this->input->get('data'));

        $item = $this->item->insert($params);
        if ($item) {
            $this->simplexml->setNodes("success", null);
            $xml = $this->simplexml->getXML(array("success" => "true"));
        } else {
            $this->errorhandler->setMessage('Something went wrong. Please try again.');
            $xml = $this->errorhandler->getError()->asXML();
        }
        $this->output->set_output($xml);
    }

    public function delete() {
        $params = (array) json_decode($this->input->post('data'));

        $item_id = $params["id"];
        $item = $this->item->delete(array("id" => $item_id));
        if ($item) {
            $this->simplexml->setNodes("success", null);
            $xml = $this->simplexml->getXML(array("success" => "true"));
        } else {
            $this->errorhandler->setMessage('Something went wrong. Please try again.');
            $xml = $this->errorhandler->getError()->asXML();
        }
        $this->output->set_output($xml);
    }

    public function sendmessage($type = "message") {
        $params = (array) json_decode($this->input->post('data'));
        // GET ITEM AND FROM USER
        $item = array_shift($this->item->fetch($params["item_id"]));
        $seller = $this->user->get(array("id" => $item['user_id']));
        $user = $this->user->get(array("id" => $params["user_id"]));
        if ($user && $item) {
            if ($type == "appointment") {
                $wishlist = $this->wishlist->insert(array("buyer_id" => $params["user_id"], "item_id" => $params["item_id"]));
                if ($wishlist) {
                    $this->notification->notify($user["email"], $user["name"], $seller["email"], "Purchase Request", $this->load->view('emailtemplate', array("title" => "Hey! Someone wants to buy your item", "content" => "<p>" . $user["name"] . " is interested in buying your item.</p><p>" . $user["name"] . " would like to meet at the " . $params["location"] . " at " . $params["time"] . ".</p><p> They also sent along this message:</p><p><blockquote>" . $params["message"] . "</blockquote></p>"), TRUE));
                    $this->simplexml->setNodes("success", null);
                    $xml = $this->simplexml->getXML(array("success" => "true"));
                } else {
                    $this->errorhandler->setMessage('Something went wrong. Please try again.');
                    $xml = $this->errorhandler->getError()->asXML();
                }
            } else {
                $this->notification->notify($user["email"], $user["name"], $seller["email"], "Item message", $this->load->view('emailtemplate', array("title" => "Hey! Someone has a message for you", "content" => $user["name"] . ":<b>" . $params["message"] . "</b>"), TRUE));
                $this->simplexml->setNodes("success", null);
                $xml = $this->simplexml->getXML(array("success" => "true"));
            }
        } else {
            $this->errorhandler->setMessage('Something went wrong. Please try again.');
            $xml = $this->errorhandler->getError()->asXML();
        }
        $this->output->set_output($xml);
    }

    public function selling($id) {
        $id = intval($id);
        $items = $this->item->get(array('user_id' => $id));
        $this->simplexml->setNodes("items", "item");
        $xml = $this->simplexml->getXML($items);
        $this->output->set_output($xml);
    }

    public function buying($id) {
        $id = intval($id);
        $items = $this->item->getWishList(array('wishlist.buyer_id' => $id));
        foreach($items as $key => $item) {
            $user = $this->user->get(array('id' => $item['user_id']));
            $item['user_name'] = $user['name'];
            $item['user_image'] = $user['image'];
            $item['user_major'] = $user['major'];

            $items[$key] = $item;
        }
        $this->simplexml->setNodes("items", "item");
        $xml = $this->simplexml->getXML($items);
        $this->output->set_output($xml);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */