<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user');
        $this->load->model('item');
        $this->load->model('school');
        $this->load->library('errorhandler');
        $this->load->library('simplexml');
        $this->load->library('notification');
        $this->output->set_content_type('text/xml');
    }

    public function index() {
        $this->fetchAll();
    }

    public function fetchAll() {
        $this->simplexml->setNodes("users", "user");
        $xml = $this->simplexml->getXML($this->user->get());
        $this->output->set_output($xml);
    }

    public function fetch($user_id = 0) {
        $this->simplexml->setNodes("users", "user");
        $xml = $this->simplexml->getXML($this->user->get(array("id" => $user_id)));
        $this->output->set_output($xml);
    }

    public function login() {
        $params = json_decode($this->input->post('data'));
        $login = $this->user->get(array("email" => $params->email, "password" => $params->password));

        if ($login) {
            $token = $this->user->update(array("session_token" => md5($login["email"] . time())), array("id" => $login["id"]));
            if ($token) {
                $user = $this->user->get(array("id" => $login["id"]));
                $this->simplexml->setNodes("user", "campus");
                $xml = $this->simplexml->getXML($user);
            } else {
                $this->errorhandler->setMessage('Something went wrong. Please try again.');
                $xml = $this->errorhandler->getError()->asXML();
            }
        } else {
            $this->errorhandler->setMessage('Invalid Login');
            $xml = $this->errorhandler->getError()->asXML();
        }
        $this->output->set_output($xml);
    }

    public function register() {
        $params = (array) json_decode($this->input->post('data'));
        $register = $this->user->insert($params);
        if ($register) {
            // GET USERS UNIVERSITY ID
            $school = $this->school->get(array("domain" => implode('.', array_slice(preg_split("/(\.|@)/", $register["email"]), -2))));
            if ($school) {
                $token = $this->user->update(array("school_id" => $school[0]["id"], "session_token" => md5($register["email"] . time())), array("id" => $register["id"]));
                if ($token) {
                    $user = $this->user->get(array("id" => $register["id"]));
                    $this->simplexml->setNodes("user", "campus");
                    $xml = $this->simplexml->getXML($user);
                } else {
                    $this->errorhandler->setMessage('Something went wrong. Please try again.');
                    $xml = $this->errorhandler->getError()->asXML();
                }
            } else {
                $this->errorhandler->setMessage('I\'m sorry, at this momment your school is not supported.');
                $xml = $this->errorhandler->getError()->asXML();
            }
        } else {
            $this->errorhandler->setMessage('Something went wrong. Please try again.');
            $xml = $this->errorhandler->getError()->asXML();
        }
        $this->output->set_output($xml);
    }

    public function update() {
        $params = (array) json_decode($this->input->post('data'));
        $user_id = $params["id"];
        $user = $this->user->update($params, array("id" => $user_id));
        if ($user) {
            $this->simplexml->setNodes("success", null);
            $xml = $this->simplexml->getXML(array("success" => "true"));
        } else {
            $this->errorhandler->setMessage('Something went wrong. Please try again.');
            $xml = $this->errorhandler->getError()->asXML();
        }
        $this->output->set_output($xml);
    }

    public function resetpassword() {
        $params = (array) json_decode($this->input->post('data'));
        $user = $this->user->get(array("email" => $params["email"]));
        if ($user) {
            $new_passord = $this->generateRandomString();
            $newpassword = $this->user->update(array("password" => $new_passord), array("id" => $user["id"]));
            if ($newpassword) {
                // SEND PASSWORD TO USER
                $this->notification->notify($this->notification->supportemail, $this->notification->supportname, $user["email"], "You're New Password", $this->load->view('emailtemplate', array("title" => "Hey! Here is your new password", "content" => "Your new pasword is: <b>" . $new_passord . "</b>"), TRUE));
                $this->simplexml->setNodes("success", null);
                $xml = $this->simplexml->getXML(array("success" => "true"));
            } else {
                $this->errorhandler->setMessage('Something went wrong. Please try again.');
                $xml = $this->errorhandler->getError()->asXML();
            }
        } else {
            $this->errorhandler->setMessage('Email not found. Please try again.');
            $xml = $this->errorhandler->getError()->asXML();
        }
        $this->output->set_output($xml);
    }

    private function generateRandomString($length = 6) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

}