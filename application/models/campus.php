<?php

class campus extends CI_Model {
    function get($where = array()) {
        foreach ($where as $key => $value) {
            $this->db->where($key, $value);
        }
        $query = $this->db->get('campus');

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }
}