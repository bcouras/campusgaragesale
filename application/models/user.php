<?php

class user extends CI_Model {

    function get($where = array()) {
        foreach ($where as $key => $value) {
            $this->db->where($key, $value);
        }
        $query = $this->db->get('user');

        if ($query->num_rows() > 0) {
            $users = array();
            $this->load->model('campus');
            foreach ($query->result_array() as $user) {
                $user["campuses"] = $this->campus->get(array("school_id" => $user["school_id"]));
                $users = $user;
            }
            return $users;
        } else {
            return array();
        }
    }

    function update($columns = array(), $where = array()) {
        foreach ($columns as $key => $value) {
            $this->db->set($key, $value);
        }
        foreach ($where as $key => $value) {
            $this->db->where($key, $value);
        }

        $this->db->update('user');
        return $this->db->affected_rows();
    }

    function insert($columns = array()) {
        foreach ($columns as $key => $value) {
            $this->db->set($key, $value);
        }

        $this->db->insert('user');

        if ($this->db->affected_rows()) {
            return $this->get(array("id" => $this->db->insert_id()));
        } else {
            return $this->db->affected_rows();
        }
    }

}