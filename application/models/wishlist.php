<?php

class wishlist extends CI_Model {

    function get($where = array()) {
        foreach ($where as $key => $value) {
            $this->db->where($key, $value);
        }
        $query = $this->db->get('wishlist');

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    function insert($columns = array()) {
        foreach ($columns as $key => $value) {
            $this->db->set($key, $value);
        }

        $this->db->insert('wishlist');

        if ($this->db->affected_rows()) {
            return $this->get(array("id" => $this->db->insert_id()));
        } else {
            return $this->db->affected_rows();
        }
    }

    function delete($where = array()) {
        foreach ($where as $key => $value) {
            $this->db->where($key, $value);
        }

        $this->db->delete('wishlist');
        return $this->db->affected_rows();
    }

}