<?php

class item extends CI_Model {

    function get($where = array()) {
        
        $this->db->select("item.*, user.name AS user_name, item.name AS name, user.image AS user_image, user.major AS user_major");
        
        foreach ($where as $key => $value) {
            $this->db->where($key, $value);
        }
        
        // JOING TO BRING OWNERS INFO
        $this->db->join('user', 'user.id = item.user_id');
        
        $query = $this->db->get('item');

        if ($query->num_rows() > 0) {
            $restuls = $query->result_array();
            foreach($restuls as $key => $item) {
                if(!$item['image']) {
                    $item['image'] = 'http://dummyimage.com/400x600/000000/fff&amp;text=Item';
                    $restuls[$key] = $item;
                }
            }
            return $restuls;
        } else {
            return array();
        }
    }

    function getWishList($where = array()) {

        $this->db->select("item.*");

        foreach ($where as $key => $value) {
            $this->db->where($key, $value);
        }

        // JOING TO BRING OWNERS INFO
        $this->db->join('wishlist', 'wishlist.item_id = item.id');

        $query = $this->db->get('item');

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    function fetch($id) {
        $id = intval($id);
        $this->db->where('id', $id);
        $query = $this->db->get('item');

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    function insert($columns = array()) {
        foreach ($columns as $key => $value) {
            $this->db->set($key, $value);
        }

        $this->db->insert('item');

        if ($this->db->affected_rows()) {
            return $this->get(array("id" => $this->db->insert_id()));
        } else {
            return $this->db->affected_rows();
        }
    }

    function delete($where = array()) {
        foreach ($where as $key => $value) {
            $this->db->where($key, $value);
        }

        $this->db->delete('item');
        return $this->db->affected_rows();
    }

}