<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Notification {

    private $CI;
    public $supportemail = "support@campusgaragesale.com";
    public $supportname = "Support Campus Garage Sale";
    private $emailsettings = array(
        'protocol' => 'smtp',
        'smtp_host' => 'smtp.sendgrid.net',
        'smtp_user' => 'brunomayerc',
        'smtp_pass' => 'UM108010',
        'smtp_port' => 587,
        'crlf' => "\r\n",
        'newline' => "\r\n",
        'mailtype' => "html"
    );

    public function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->library('email');
    }

    public function notify($from, $from_name, $to, $subject, $message) {
        $this->CI->email->initialize($this->emailsettings);
        $this->CI->email->from($from, $from_name);
        $this->CI->email->to($to);
        $this->CI->email->subject($subject);
        $this->CI->email->message($message);
        $this->CI->email->send();
    }

}