<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Simplexml {

    private $root = "root";
    private $child = null;
    private $xml;

    public function setNodes($root, $child) {
        $this->root = $root;
        $this->child = $child;
        $this->xml = new SimpleXMLElement("<?xml version=\"1.0\"?><" . $this->root . "></" . $this->root . ">");
    }

    public function getXML($array) {
        $this->array_to_xml($array, $this->xml);
        return $this->xml->asXML();
    }

    private function array_to_xml($array, &$xml) {
        if(!is_array($array)){
            $array = array();
        }
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                if (!is_numeric($key)) {
                    $subnode = $xml->addChild("$key");
                    $this->array_to_xml($value, $subnode);
                } else {
                    if ($this->child) {
                        $subnode = $xml->addChild($this->child);
                    } else {
                        $subnode = $xml->addChild("item");
                    }
                    $this->array_to_xml($value, $subnode);
                }
            } else {
                $xml->addChild("$key", "$value");
            }
        }
    }

}