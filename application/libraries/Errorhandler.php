<?php

class Errorhandler {

    /**
     * @var SimpleXMLElement;
     */
    public $error;

    public function __construct(){
        $this->error = simplexml_load_string('<error/>');
    }

    /**
     * Set the error message
     * @param $message
     * @return $this
     */
    public function setMessage($message) {
        if(!$this->error->message) {
            $this->error->addChild('message');
        }
        $this->error->message = $message;
        return $this;
    }

    /**
     * Set optional parameters
     * @param array $params
     * @return $this
     */
    public function setParams(array $params) {
        if($this->error->params){
            unset($this->error->params);
        }
        $i = 0;
        $this->error->addChild('params');
        foreach($params as $key => $value) {
            $this->error->params->addChild('param');
            $this->error->params->param[$i]->addChild('name', $key);
            $this->error->params->param[$i]->addChild('value', $value);
            $i++;
        }
        return $this;
    }

    /**
     * @return SimpleXMLElement
     */
    public function getError(){
        return $this->error;
    }
}